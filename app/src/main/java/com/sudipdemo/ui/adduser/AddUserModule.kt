package com.sudipdemo.ui.adduser

import androidx.lifecycle.ViewModelProvider

import com.sudipdemo.ViewModelProviderFactory
import com.sudipdemo.data.SudipDemoDataManager
import com.sudipdemo.utils.rx.SchedulerProvider


import dagger.Module
import dagger.Provides

@Module
class AddUserModule {

    @Provides
    internal fun splashViewModelProvider(splashViewModel: AddUserViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(splashViewModel)
    }

    @Provides
    internal fun splashViewModel(seedDataManager: SudipDemoDataManager, schedulerProvider: SchedulerProvider): AddUserViewModel {
        return AddUserViewModel(seedDataManager, schedulerProvider)
    }
}
