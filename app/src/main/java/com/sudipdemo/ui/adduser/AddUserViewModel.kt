package com.sudipdemo.ui.adduser


import android.util.Log
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.sudipdemo.data.SudipDemoDataManager
import com.sudipdemo.data.model.api.CommonRequest
import com.sudipdemo.data.model.db.User
import com.sudipdemo.ui.base.BaseViewModel
import com.sudipdemo.utils.Resource
import com.sudipdemo.utils.rx.SchedulerProvider

class AddUserViewModel(dataManager: SudipDemoDataManager, schedulerProvider: SchedulerProvider)//        IsUser();
    : BaseViewModel(dataManager, schedulerProvider) {
    private val response = MutableLiveData<Resource<Boolean>>()


    fun isValidPhone(phone: String): Boolean {
        return Patterns.PHONE.matcher(phone).matches()
    }
    fun isValidEmail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }




    fun insertUser(user: User) {
        response.postValue(Resource.loading(null))
        setIsLoading(true)
        compositeDisposable.add(dataManager
                .insertUser(user)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({ insertResponse ->
                    if (insertResponse != null ) {
                        Log.d("response", "details" + Gson().toJson(insertResponse))

                        if (insertResponse!!) {
                           response.postValue(Resource.success(insertResponse))

                        } else {
                            response.postValue(Resource.fail(insertResponse))

                        }

                    }
                    setIsLoading(false)
                }, { throwable ->
                    setIsLoading(false)
                    Log.d("handleErrorApi",""+throwable.message)


                }))


    }



    fun getResponse(): LiveData<Resource<Boolean>> {
        return response
    }

}
