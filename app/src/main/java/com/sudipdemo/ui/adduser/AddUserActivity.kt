package com.sudipdemo.ui.adduser

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.sudipdemo.BR
import com.sudipdemo.R
import com.sudipdemo.data.model.db.User
import com.sudipdemo.databinding.ActivityAdduserBinding
import com.sudipdemo.databinding.ActivityFirstscreenBinding
import com.sudipdemo.ui.base.BaseActivity
import com.sudipdemo.utils.Status
import javax.inject.Inject
import kotlin.random.Random


class AddUserActivity : BaseActivity<ActivityAdduserBinding, AddUserViewModel>() {




    @Inject
    internal lateinit var mViewModelFactory: ViewModelProvider.Factory
    @Inject
    internal lateinit var addUserViewModel: AddUserViewModel
    internal lateinit var activityAdduserBinding: ActivityAdduserBinding


    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.activity_adduser

    override val viewModel: AddUserViewModel
        get() {
            addUserViewModel = ViewModelProvider(this, mViewModelFactory).get(AddUserViewModel::class.java)
            return addUserViewModel
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityAdduserBinding = viewDataBinding!!

        checkvalidationAndSaveData()




    }

    private fun checkvalidationAndSaveData() {
        activityAdduserBinding.btnSubmit.setOnClickListener {
            if(activityAdduserBinding.etFirstname.text.isNullOrEmpty()){
                activityAdduserBinding.etFirstname.requestFocus()
                showToast(this,getString(R.string.please_enter_first_name))
            }
            else if(activityAdduserBinding.etAddress.text.isNullOrEmpty()){
                activityAdduserBinding.etAddress.requestFocus()
                showToast(this,getString(R.string.please_enter_your_address))
            }else if(activityAdduserBinding.etPhone.text.isNullOrEmpty()
                    || !addUserViewModel.isValidPhone(activityAdduserBinding.etPhone.text.toString())
            ){
                activityAdduserBinding.etPhone.requestFocus()
                showToast(this,getString(R.string.please_enter_valid_phone_number))
            }
            else if(activityAdduserBinding.etEmail.text.isNullOrEmpty()
                    || !addUserViewModel.isValidEmail(activityAdduserBinding.etEmail.text.toString())
            ){
                activityAdduserBinding.etEmail.requestFocus()
                showToast(this,getString(R.string.please_enter_valid_email_id))
            }else{

                Log.d("new","--"+"INSIDE")
                var user :   User = User()
                user.id=  Random(100).toString()
                user.phone=activityAdduserBinding.etPhone.text.toString()
                user.name=activityAdduserBinding.etFirstname.text.toString()
                user.email=activityAdduserBinding.etEmail.text.toString()
                user.address=activityAdduserBinding.etAddress.text.toString()

                addUserViewModel.insertUser(user!!)




            }

        }

        setlivedataresponse()
    }

    private fun setlivedataresponse() {



        addUserViewModel.getResponse().observe(this, androidx.lifecycle.Observer {

            when (it.status) {
                Status.SUCCESS -> {

                    Log.d("succesiinser","--")
                    hideLoading()
                     showToast(this,""+"data successfully inserted")
                    onBackPressed()




                }
                Status.FAIL -> {
                    hideLoading()



//
                }
                Status.LOADING -> {
                    showLoading()
                }
                Status.ERROR -> {
                    //Handle Error
                    hideLoading()
                    Toast.makeText(this , getString(R.string.server_error) , Toast.LENGTH_LONG).show()
                    Log.d("handleError" , "" + it.message)
                }
            }

        })

    }



}
