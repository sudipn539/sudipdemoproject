package com.sudipdemo.ui.userlist

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.sudipdemo.BR
import com.sudipdemo.R
import com.sudipdemo.data.model.db.User
import com.sudipdemo.databinding.ActivityFirstscreenBinding
import com.sudipdemo.databinding.ActivityUserlistBinding
import com.sudipdemo.ui.adduser.AddUserActivity
import com.sudipdemo.ui.base.BaseActivity
import com.sudipdemo.ui.userlist.adapter.UserListAdapter
import com.sudipdemo.utils.Status
import java.util.*
import javax.inject.Inject


class UserListActivity : BaseActivity<ActivityUserlistBinding, UserListViewModel>() {




    @Inject
    internal lateinit var mViewModelFactory: ViewModelProvider.Factory
    @Inject
    internal lateinit var userListViewModel: UserListViewModel
    internal lateinit var activityUserlistBinding: ActivityUserlistBinding


    lateinit var userListAdapter:UserListAdapter

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.activity_userlist

    override val viewModel: UserListViewModel
        get() {
            userListViewModel = ViewModelProvider(this, mViewModelFactory).get(UserListViewModel::class.java)
            return userListViewModel
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityUserlistBinding = viewDataBinding!!
        setuserListRecycle()
        userListViewModel.fetchUser()
        getLiveData()

    }


    private fun setuserListRecycle() {
            var list =LinkedList<User>()
        userListAdapter = UserListAdapter(list,this)
        activityUserlistBinding.userlistRcv.layoutManager = LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,
                false)
        activityUserlistBinding.userlistRcv.adapter = userListAdapter
    }

    private fun getLiveData() {

        userListViewModel.getResponseList().observe(this, androidx.lifecycle.Observer {

            when (it.status) {
                Status.SUCCESS -> {
                hideLoading()

                    it.data?.let { it1 -> userListAdapter.addItems(it1) }



                }
                Status.FAIL -> {
                    hideLoading()



//
                }
                Status.LOADING -> {
                    showLoading()
                }
                Status.ERROR -> {
                    //Handle Error
                    hideLoading()
                    Toast.makeText(this , getString(R.string.server_error) , Toast.LENGTH_LONG).show()
//        Toast.makeText(this,""+throwable.localizedMessage,Toast.LENGTH_LONG).show()
                    Log.d("handleError" , "" + it.message)
                }
            }

        })

    }

}
