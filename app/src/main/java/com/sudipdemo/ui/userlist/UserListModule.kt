package com.sudipdemo.ui.userlist

import androidx.lifecycle.ViewModelProvider

import com.sudipdemo.ViewModelProviderFactory
import com.sudipdemo.data.SudipDemoDataManager
import com.sudipdemo.utils.rx.SchedulerProvider


import dagger.Module
import dagger.Provides

@Module
class UserListModule {

    @Provides
    internal fun splashViewModelProvider(splashViewModel: UserListViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(splashViewModel)
    }

    @Provides
    internal fun splashViewModel(seedDataManager: SudipDemoDataManager, schedulerProvider: SchedulerProvider): UserListViewModel {
        return UserListViewModel(seedDataManager, schedulerProvider)
    }
}
