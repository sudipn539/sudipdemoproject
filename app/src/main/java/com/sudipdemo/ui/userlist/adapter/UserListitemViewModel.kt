package com.sudipdemo.ui.userlist.adapter


import android.content.Context
import androidx.databinding.ObservableField
import com.sudipdemo.data.model.db.User
import com.sudipdemo.databinding.ItemUserlistBinding

import java.util.*


class UserListitemViewModel(items: List<User>, val position: Int, val mBinding: ItemUserlistBinding, val context : Context) {

    var name: ObservableField<String>
    var address: ObservableField<String>
    var phone: ObservableField<String>

    var email: ObservableField<String>


    init {
        name= ObservableField(items[position].name!!.capitalize())
        address= ObservableField(items[position].address!!)

        phone= ObservableField(items[position].phone!!)
        email= ObservableField(items[position].email!!)


    }


}
