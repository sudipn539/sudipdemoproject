package com.sudipdemo.ui.userlist.adapter
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.sudipdemo.data.model.db.User
import com.sudipdemo.databinding.ItemUserlistBinding
import com.sudipdemo.ui.base.BaseViewHolder

import java.util.*


class UserListAdapter(private val muserResponseList: LinkedList<User>, val context :Context) : RecyclerView.Adapter<BaseViewHolder>() {




    override fun getItemCount(): Int {
        return muserResponseList.size
    }



var callback: AdapterView.OnItemClickListener? = null

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): BaseViewHolder {


        val blogViewBinding = ItemUserlistBinding.inflate(LayoutInflater.from(parent.context),
                parent, false)
        return HomeViewHolder(blogViewBinding)

    }

    override fun getItemViewType(position: Int): Int {

        return VIEW_TYPE_NORMAL

    }

    companion object {

        val VIEW_TYPE_NORMAL = 1
    }




    override fun onBindViewHolder(@NonNull holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }



    inner class HomeViewHolder(private val mBinding: ItemUserlistBinding) : BaseViewHolder(mBinding.getRoot()) {





        override fun onBind(position: Int) {
            //     val order = muserResponseList[position]


            var mRecentReviewsListitemViewModel = UserListitemViewModel(muserResponseList!!,position,mBinding,context)
            mBinding.viewModel =mRecentReviewsListitemViewModel
            //mBinding.executePendingBindings()



        }

    }




    fun addItems(rderResponseList: List<User>)
    {
        muserResponseList.addAll(rderResponseList)
        notifyDataSetChanged()
    }



}
