package com.sudipdemo.ui.userlist


import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.sudipdemo.data.SudipDemoDataManager
import com.sudipdemo.data.model.db.User
import com.sudipdemo.ui.base.BaseViewModel
import com.sudipdemo.utils.Resource
import com.sudipdemo.utils.rx.SchedulerProvider

class UserListViewModel(dataManager: SudipDemoDataManager, schedulerProvider: SchedulerProvider)//        IsUser();
    : BaseViewModel(dataManager, schedulerProvider) {
    private val responseList = MutableLiveData<Resource<List<User>>>()



    fun fetchUser() {
        responseList.postValue(Resource.loading(null))
        setIsLoading(true)
        compositeDisposable.add(dataManager
                .allUsers
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({ userlistResponse ->
                    if (userlistResponse != null ) {
                        Log.d("response", "details" + Gson().toJson(userlistResponse))

                        if (userlistResponse.isNotEmpty()!!) {
                            responseList.postValue(Resource.success(userlistResponse))

                        } else {
                            responseList.postValue(Resource.fail(userlistResponse))

                        }

                    }
                    setIsLoading(false)
                }, { throwable ->
                    setIsLoading(false)
                    Log.d("handleErrorApi",""+throwable.message)
                    // response.postValue(Resource.error(""+throwable.message, null))


                }))


    }

    fun getResponseList(): LiveData<Resource<List<User>>> {
        return responseList
    }


}
