package com.sudipdemo.ui.firstscreen

import android.annotation.TargetApi
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.sudipdemo.BR
import com.sudipdemo.R
import com.sudipdemo.databinding.ActivityFirstscreenBinding
import com.sudipdemo.ui.adduser.AddUserActivity
import com.sudipdemo.ui.base.BaseActivity
import com.sudipdemo.ui.userlist.UserListActivity
import javax.inject.Inject


class FirstScreenActivity : BaseActivity<ActivityFirstscreenBinding, FirstScreenViewModel>() {




    @Inject
    internal lateinit var mViewModelFactory: ViewModelProvider.Factory
    @Inject
    internal lateinit var firstScreenViewModel: FirstScreenViewModel
    internal lateinit var activityFirstscreenBinding: ActivityFirstscreenBinding


    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.activity_firstscreen

    override val viewModel: FirstScreenViewModel
        get() {
            firstScreenViewModel = ViewModelProvider(this, mViewModelFactory).get(FirstScreenViewModel::class.java)
            return firstScreenViewModel
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityFirstscreenBinding = viewDataBinding!!


        activityFirstscreenBinding.adduserTv.setOnClickListener {
            startActivity(Intent(this,AddUserActivity::class.java))
        }

        activityFirstscreenBinding.userlistTv.setOnClickListener {
            startActivity(Intent(this,UserListActivity::class.java))
        }



    }

}
