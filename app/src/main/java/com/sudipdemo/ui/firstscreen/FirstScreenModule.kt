package com.sudipdemo.ui.firstscreen

import androidx.lifecycle.ViewModelProvider

import com.sudipdemo.ViewModelProviderFactory
import com.sudipdemo.data.SudipDemoDataManager
import com.sudipdemo.utils.rx.SchedulerProvider


import dagger.Module
import dagger.Provides

@Module
class FirstScreenModule {

    @Provides
    internal fun splashViewModelProvider(splashViewModel: FirstScreenViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(splashViewModel)
    }

    @Provides
    internal fun splashViewModel(seedDataManager: SudipDemoDataManager, schedulerProvider: SchedulerProvider): FirstScreenViewModel {
        return FirstScreenViewModel(seedDataManager, schedulerProvider)
    }
}
