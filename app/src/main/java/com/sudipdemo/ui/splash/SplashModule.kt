package com.sudipdemo.ui.splash

import androidx.lifecycle.ViewModelProvider

import com.sudipdemo.ViewModelProviderFactory
import com.sudipdemo.data.SudipDemoDataManager
import com.sudipdemo.utils.rx.SchedulerProvider


import dagger.Module
import dagger.Provides

@Module
class SplashModule {

    @Provides
    internal fun splashViewModelProvider(splashViewModel: SplashViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(splashViewModel)
    }

    @Provides
    internal fun splashViewModel(seedDataManager: SudipDemoDataManager, schedulerProvider: SchedulerProvider): SplashViewModel {
        return SplashViewModel(seedDataManager, schedulerProvider)
    }
}
