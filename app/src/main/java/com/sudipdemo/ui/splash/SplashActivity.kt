package com.sudipdemo.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.ViewModelProvider
import com.sudipdemo.BR
import com.sudipdemo.R
import com.sudipdemo.databinding.ActivitySplashBinding
import com.sudipdemo.ui.base.BaseActivity
import com.sudipdemo.ui.firstscreen.FirstScreenActivity
import javax.inject.Inject


class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {





    @Inject
    internal lateinit var mViewModelFactory: ViewModelProvider.Factory
    @Inject
    internal lateinit var splashViewModel: SplashViewModel
    internal lateinit var activityAddProductBinding: ActivitySplashBinding


    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.activity_splash

    override val viewModel: SplashViewModel
        get() {
            splashViewModel = ViewModelProvider(this, mViewModelFactory).get(SplashViewModel::class.java)
            return splashViewModel
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityAddProductBinding = viewDataBinding!!

            Handler().postDelayed({
                    startActivity(Intent(this@SplashActivity , FirstScreenActivity::class.java))
                    finish()

            }, 2000)



    }

}
