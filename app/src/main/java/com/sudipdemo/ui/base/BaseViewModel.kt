package com.sudipdemo.ui.base


import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import com.sudipdemo.data.SudipDemoDataManager
import com.sudipdemo.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel(val dataManager: SudipDemoDataManager, val schedulerProvider: SchedulerProvider) : ViewModel() {

    val isLoading = ObservableBoolean(false)

    val compositeDisposable: CompositeDisposable

//    private var mNavigator: WeakReference<N>? = null
//
//    var navigator: N
//        get() = mNavigator!!.get()!!
//        set(navigator) {
//            mNavigator = WeakReference(navigator)
//        }

    init {
        this.compositeDisposable = CompositeDisposable()
    }
    //    public BaseViewModel( SchedulerProvider schedulerProvider) {
    //
    //        this.mSchedulerProvider = schedulerProvider;
    //        this.mCompositeDisposable = new CompositeDisposable();
    //    }

    override fun onCleared() {
        Log.d("onCleared","onCleared")

        compositeDisposable.dispose()
        super.onCleared()
    }

    fun setIsLoading(isLoading: Boolean) {
        this.isLoading.set(isLoading)
    }
}
