package com.sudipdemo.ui.base


import android.annotation.TargetApi
import android.app.Dialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.material.snackbar.Snackbar
import com.sudipdemo.utils.CommonUtils
import com.sudipdemo.utils.NetworkUtils
import dagger.android.AndroidInjection

abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity(), BaseFragment.Callback {

    // TODO
    // this can probably depend on isLoading variable of BaseViewModel,
    // since its going to be common for all the activities
//    private var mProgressDialog: ProgressDialog? = null
    private var mProgressDialog: Dialog? = null

    var viewDataBinding: T? = null
        private set
    private var mViewModel: V? = null

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract val bindingVariable: Int

    /**
     * @return layout resource id
     */
    @get:LayoutRes
    abstract val layoutId: Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract val viewModel: V

    val isNetworkConnected: Boolean
        get() = NetworkUtils.isNetworkConnected(applicationContext)

    override fun onFragmentAttached() {
             hideLoading()
    }

    override fun onFragmentDetached(tag: String) {
            hideLoading()
    }

//    override fun attachBaseContext(newBase: Context) {
//        super.attachBaseContext(updateBaseContextLocale(newBase))
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        performDependencyInjection()
        super.onCreate(savedInstanceState)
        performDataBinding()
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun hasPermission(permission: String): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)

        }
    }

    fun hideLoading() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing) {
            mProgressDialog!!.dismiss()
        }
    }

    fun openActivityOnTokenExpire() {
        //        startActivity(VerifyPhoneActivity.newIntent(this));
        //        finish();
    }

    fun performDependencyInjection() {
        AndroidInjection.inject(this)
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun requestPermissionsSafely(permissions: Array<String>, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode)
        }
    }

    fun showLoading() {
        hideLoading()
//        mProgressDialog = CommonUtils.setProgressDialog(this)
        mProgressDialog = CommonUtils.showLoadingDialog1(this)
//                CommonUtils.showLoadingDialog(this)
    }

    private fun performDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, layoutId)

        this.mViewModel = if (mViewModel == null) viewModel else mViewModel
        viewDataBinding!!.setVariable(bindingVariable, mViewModel)
        viewDataBinding!!.executePendingBindings()

    }
    open fun printLog(tag: String , message: String) {
        Log.d(tag , message)
    }

    /**
     * @param v           A View in which the SnackBar should be displayed at the bottom of the
     * screen.
     * @param stringResID A message that to be displayed inside a SnackBar.
     */
    open fun showSnackBar(v: View , @StringRes stringResID: Int) {
        Snackbar.make(v , stringResID , Snackbar.LENGTH_LONG).show()
    }

    /**
     * @param context An Activity or Application Context.
     * @param message A message that to be displayed inside a Toast.
     */
    open fun showToast(context: Context , message: String) {
        Toast.makeText(context , message , Toast.LENGTH_LONG).show()
    }
//     open fun updateBaseContextLocale(context: Context): Context? {
//
//        //String language = SharedPrefUtils.getSavedLanguage(); // Helper method to get saved language from SharedPreferences
//        val locale = Locale("it_IT", "IT")
//        //        Locale locale = new Locale("en_US","US");
//        Locale.setDefault(locale)
//        Log.d("log", "00" + Locale.getDefault().country)
//        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            updateResourcesLocale(context, locale)
//        } else updateResourcesLocaleLegacy(context, locale)
//    }

//    @TargetApi(Build.VERSION_CODES.N)
//     open fun updateResourcesLocale(context: Context, locale: Locale): Context? {
//        val configuration = context.resources.configuration
//        configuration.setLocale(locale)
//        return context.createConfigurationContext(configuration)
//    }

//     open fun updateResourcesLocaleLegacy(context: Context, locale: Locale): Context? {
//        val resources = context.resources
//        val configuration = resources.configuration
//        configuration.locale = locale
//        resources.updateConfiguration(configuration, resources.displayMetrics)
//        return context
//    }

}
