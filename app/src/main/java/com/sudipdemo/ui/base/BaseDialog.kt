package com.sudipdemo.ui.base

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.snackbar.Snackbar


abstract class BaseDialog : DialogFragment() {
    var baseActivity: BaseActivity<*, *>? = null

    val isNetworkConnected: Boolean
        get() = baseActivity != null && baseActivity!!.isNetworkConnected

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            val mActivity = context as BaseActivity<*, *>?
            this.baseActivity = mActivity
            mActivity!!.onFragmentAttached()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // the content
        val root = ConstraintLayout(activity)
        root.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)


        // creating the fullscreen dialog
        val dialog = Dialog(baseActivity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        if (dialog.window != null) {
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            dialog.window!!.setLayout(
//                    ViewGroup.LayoutParams.WRAP_CONTENT,
//                    ViewGroup.LayoutParams.WRAP_CONTENT)
//            dialog.window!!.isFloating=true

//            dialog.leftMargin = 16
//            (view!!.layoutParams as ViewGroup.MarginLayoutParams).rightMargin = 16
            dialog.window!!.setGravity(Gravity.CENTER)
        }
        dialog.setCanceledOnTouchOutside(true)

        dialog.setOnCancelListener {
            shown=false
        }
        return dialog
    }

    override fun onResume() {
        super.onResume()
        dialog?.setOnKeyListener { dialog, keyCode, event ->
            if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
                // To dismiss the fragment when the back-button is pressed.
                shown = false
                dismiss()
                true
            } else
                false// Otherwise, do nothing else
        }

    }

    override fun onDetach() {
        baseActivity = null
        super.onDetach()
    }


    override fun show(fragmentManager: FragmentManager, tag: String?) {
        if (shown) return
        val transaction = fragmentManager.beginTransaction()
        val prevFragment = fragmentManager.findFragmentByTag(tag)
        if (prevFragment != null) {
            transaction.remove(prevFragment)
        }
        transaction.addToBackStack(null)
        show(transaction, tag)
        shown = true
    }

    fun dismissDialog(tag: String) {
        shown = false
        dismiss()
        baseActivity!!.onFragmentDetached(tag)
    }

    fun hideKeyboard() {
        if (baseActivity != null) {
            baseActivity!!.hideKeyboard()
        }
    }

    fun hideLoading() {
        if (baseActivity != null) {
            baseActivity!!.hideLoading()
        }
    }

    fun openActivityOnTokenExpire() {
        if (baseActivity != null) {
            baseActivity!!.openActivityOnTokenExpire()
        }
    }

    fun showLoading() {
        if (baseActivity != null) {
            baseActivity!!.showLoading()
        }
    }

    open fun printLog(tag: String , message: String) {
        Log.d(tag , message)
    }

    /**
     * @param v           A View in which the SnackBar should be displayed at the bottom of the
     * screen.
     * @param stringResID A message that to be displayed inside a SnackBar.
     */
    open fun showSnackBar(v: View , @StringRes stringResID: Int) {
        Snackbar.make(v , stringResID , Snackbar.LENGTH_LONG).show()
    }

    /**
     * @param context An Activity or Application Context.
     * @param message A message that to be displayed inside a Toast.
     */
    open fun showToast(context: Context , message: String) {
        Toast.makeText(context , message , Toast.LENGTH_LONG).show()
    }




    companion object {
        private var shown = false
    }
}
