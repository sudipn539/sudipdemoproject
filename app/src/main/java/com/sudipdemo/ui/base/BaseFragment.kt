package com.sudipdemo.ui.base


import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.sudipdemo.utils.CommonUtils
import dagger.android.support.AndroidSupportInjection


abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel> : Fragment() {

    var baseActivity: BaseActivity<*, *>? = null
//        private set
    private var mRootView: View? = null
    var viewDataBinding: T? = null
//        private set
    private var mViewModel: V? = null
//    private var mProgressDialog: ProgressDialog? = null
    private var mProgressDialog: Dialog? = null

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract val bindingVariable: Int

    /**
     * @return layout resource id
     */
    @get:LayoutRes
    abstract val layoutId: Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract val viewModel: V

    val isNetworkConnected: Boolean
        get() = baseActivity != null && baseActivity!!.isNetworkConnected

    override fun onAttach(context: Context) {
        hideLoading()
        super.onAttach(context)
        if (context is BaseActivity<* , *>) {
            val activity = context
            this.baseActivity = activity
            activity.onFragmentAttached()
        }
//        if (context is BaseActivity<*, *>) {
//            val activity = context as BaseActivity<*, *>?
//            this.baseActivity = activity
//            activity!!.onFragmentAttached()
//        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        performDependencyInjection()
        super.onCreate(savedInstanceState)
        mViewModel = viewModel
        setHasOptionsMenu(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        viewDataBinding!!.lifecycleOwner = this
        mRootView = viewDataBinding!!.root
        return mRootView
    }

    override fun onDetach() {
        baseActivity = null
        hideLoading()
        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding!!.setVariable(bindingVariable, mViewModel)
        viewDataBinding!!.executePendingBindings()
    }

    fun hideKeyboard() {
        if (baseActivity != null) {
            baseActivity!!.hideKeyboard()
        }
    }

    fun openActivityOnTokenExpire() {
        if (baseActivity != null) {
            baseActivity!!.openActivityOnTokenExpire()
        }
    }

    private fun performDependencyInjection() {
        AndroidSupportInjection.inject(this)
//        AndroidInjection.inject(baseActivity)
    }

    interface Callback {

        fun onFragmentAttached()

        fun onFragmentDetached(tag: String)
    }

    fun showLoading() {
//        if (activity!=null && isAdded) {
            if (baseActivity != null && !baseActivity!!.isFinishing) {
                baseActivity!!.runOnUiThread {
                    hideLoading()
                    mProgressDialog = CommonUtils.showLoadingDialog1(baseActivity!!)
                }
            }
//        }
    }

    fun hideLoading() {
//        if (activity!=null && isAdded) {
            if (mProgressDialog != null && mProgressDialog!!.isShowing) {
                mProgressDialog!!.dismiss()
            }
//        }
    }
    open fun printLog(tag: String , message: String) {
        Log.d(tag , message)
    }

    /**
     * @param v           A View in which the SnackBar should be displayed at the bottom of the
     * screen.
     * @param stringResID A message that to be displayed inside a SnackBar.
     */
    open fun showSnackBar(v: View , @StringRes stringResID: Int) {
        Snackbar.make(v , stringResID , Snackbar.LENGTH_LONG).show()
    }

    /**
     * @param context An Activity or Application Context.
     * @param message A message that to be displayed inside a Toast.
     */
    open fun showToast(context: Context , message: String) {
        Toast.makeText(context , message , Toast.LENGTH_LONG).show()
    }

}

