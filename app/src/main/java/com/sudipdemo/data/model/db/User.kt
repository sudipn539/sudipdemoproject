package com.sudipdemo.data.model.db

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "users")
class User() : Parcelable {


    @PrimaryKey
    @SerializedName("id")
    @Expose
    @NonNull
    var id: String? = null

    @ColumnInfo(name ="email")
    @SerializedName("email")
    @Expose
    var email: String? = null

    @ColumnInfo(name ="phone")
    @SerializedName("phone")
    @Expose
    var phone: String? = null

    @ColumnInfo(name ="name")
    @SerializedName("name")
    @Expose
    var name: String? = null

    @ColumnInfo(name ="address")
    @SerializedName("address")
    @Expose
    var address: String? = null


//

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        email = parcel.readString()
        phone = parcel.readString()
        name = parcel.readString()
        address = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(email)
        parcel.writeString(phone)
        parcel.writeString(name)
        parcel.writeString(address)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }


}


