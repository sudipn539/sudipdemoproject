package com.sudipdemo.data.model.api

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class CommonRequest private constructor()//This class is not publicly instantiable
    : Parcelable {


    constructor(parcel: Parcel) : this() {
    }

    class ServerCommonRequest() : Parcelable {




        @Expose
        @SerializedName("receiverid")
        var receiverid: String? = null


        @Expose
        @SerializedName("senderid")
        var senderid: String? = null





        @Expose
        @SerializedName("verify_code")
        var verify_code: String? = null

        @Expose
        @SerializedName("device_token")
        var device_token: String? = null
        @Expose
        @SerializedName("device_type")
        var device_type: String? = null

        @Expose
        @SerializedName("latitude")
        var latitude: String? = null



        @Expose
        @SerializedName("longitude")
        var longitude: String? = null

        @Expose
        @SerializedName("user_id")
        var user_id: String? = null

        @Expose
        @SerializedName("message")
        var message: String? = null

        @Expose
        @SerializedName("name")
        var name: String? = null

        @Expose
        @SerializedName("surname")
        var surname: String? = null


        @Expose
        @SerializedName("fiscal_Code")
        var fiscal_Code: String? = null

        @Expose
        @SerializedName("identity_card_number")
        var identity_card_number: String? = null

        @Expose
        @SerializedName("email")
        var email: String? = null

        @Expose
        @SerializedName("phone_number")
        var phone_number: String? = null


        @Expose
        @SerializedName("dob")
        var dob: String? = null


        @Expose
        @SerializedName("state_id")
        var state_id: String? = null

        constructor(parcel: Parcel) : this() {
            receiverid = parcel.readString()
            senderid = parcel.readString()
            verify_code = parcel.readString()
            device_token = parcel.readString()
            device_type = parcel.readString()
            latitude = parcel.readString()
            longitude = parcel.readString()
            user_id = parcel.readString()
            message = parcel.readString()
            name = parcel.readString()
            surname = parcel.readString()
            fiscal_Code = parcel.readString()
            identity_card_number = parcel.readString()
            email = parcel.readString()
            phone_number = parcel.readString()
            dob = parcel.readString()
            state_id = parcel.readString()
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(receiverid)
            parcel.writeString(senderid)
            parcel.writeString(verify_code)
            parcel.writeString(device_token)
            parcel.writeString(device_type)
            parcel.writeString(latitude)
            parcel.writeString(longitude)
            parcel.writeString(user_id)
            parcel.writeString(message)
            parcel.writeString(name)
            parcel.writeString(surname)
            parcel.writeString(fiscal_Code)
            parcel.writeString(identity_card_number)
            parcel.writeString(email)
            parcel.writeString(phone_number)
            parcel.writeString(dob)
            parcel.writeString(state_id)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<ServerCommonRequest> {
            override fun createFromParcel(parcel: Parcel): ServerCommonRequest {
                return ServerCommonRequest(parcel)
            }

            override fun newArray(size: Int): Array<ServerCommonRequest?> {
                return arrayOfNulls(size)
            }
        }

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CommonRequest> {
        override fun createFromParcel(parcel: Parcel): CommonRequest {
            return CommonRequest(parcel)
        }

        override fun newArray(size: Int): Array<CommonRequest?> {
            return arrayOfNulls(size)
        }
    }
}
