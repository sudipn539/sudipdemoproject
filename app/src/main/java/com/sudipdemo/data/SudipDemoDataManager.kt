package com.sudipdemo.data

import com.sudipdemo.data.local.db.DbHelper
import com.sudipdemo.data.local.pref.SudipDemoPrefHelper
import com.sudipdemo.data.remote.ApiHelper

interface SudipDemoDataManager : DbHelper, ApiHelper, SudipDemoPrefHelper {

    fun setUserAsLoggedOut()

    fun updateApiHeader(userId: String, accessToken: String)

    fun updateUserInfo(
            accessToken: String,
            userId: String,
            loggedInMode: LoggedInMode,
            userName: String,
            email: String,
            profilePicPath: String,
            passWord:String,fiscalcode:String,idcardNo:String,phone:String)

    enum class LoggedInMode  constructor(val type: Int) {

        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_GOOGLE(1),
        LOGGED_IN_MODE_FB(2),
        LOGGED_IN_MODE_SERVER(3)
    }
}
