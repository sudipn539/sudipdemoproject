package com.sudipdemo.data.remote


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.sudipdemo.SudipDemo
import com.sudipdemo.di.ApiInfo
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ApiHeader @Inject
constructor(val publicApiHeader: PublicApiHeader, val protectedApiHeader: ProtectedApiHeader) {



    class ProtectedApiHeader(@field:Expose
                             @field:SerializedName("x-api-key")
                             var apiKey: String?=null,
                             @field:Expose
                             @field:SerializedName("user_id")
                             var userId: String?=null,
                             @field:Expose
                             @field:SerializedName("access_token")
                             var accessToken: String?=null,
                             @field:Expose
                             @field:SerializedName("timezone")
                             var timezone: String? = null):Parcelable {


        init {
            this.timezone = SudipDemo.mInstance.getTimezone()
            println("HeaderTimejon $timezone")
        }
        constructor(parcel: Parcel) : this(
                parcel.readString() ,
                parcel.readString() ,
                parcel.readString() ,
                parcel.readString()) {
        }

        override fun writeToParcel(parcel: Parcel , flags: Int) {
            parcel.writeString(apiKey)
            parcel.writeString(userId)
            parcel.writeString(accessToken)
            parcel.writeString(timezone)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<ProtectedApiHeader> {
            override fun createFromParcel(parcel: Parcel): ProtectedApiHeader {
                return ProtectedApiHeader(parcel)
            }

            override fun newArray(size: Int): Array<ProtectedApiHeader?> {
                return arrayOfNulls(size)
            }
        }
    }

    class PublicApiHeader @Inject
    constructor(@param:ApiInfo
                @field:Expose
                @field:SerializedName("x-api-key")
                var apiKey: String)


}
