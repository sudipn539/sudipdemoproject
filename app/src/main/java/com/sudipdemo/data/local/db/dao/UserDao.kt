package com.sudipdemo.data.local.db.dao

import androidx.room.*
import com.sudipdemo.data.model.db.User
import io.reactivex.Single


@Dao
interface UserDao {



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)


    @Query("SELECT * FROM users  order by name asc")
    fun loadAll(): List<User>


}
