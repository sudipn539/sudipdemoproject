package com.sudipdemo.data.local.db


import com.sudipdemo.data.model.db.User
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppDbHelper @Inject
constructor(private val appDataBase: AppDataBase) : DbHelper {



    override val allUsers: Observable<List<User>>
        get() = Observable.fromCallable { appDataBase.userDao().loadAll()

        }

    override fun insertUser(user: User): Observable<Boolean> {
        return Observable.fromCallable {
            appDataBase.userDao().insert(user)
            true
        }
    }

}
