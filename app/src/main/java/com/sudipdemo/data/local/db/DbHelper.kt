package com.sudipdemo.data.local.db

import com.sudipdemo.data.model.db.User
import io.reactivex.Observable


interface DbHelper {

     val allUsers: Observable<List<User>>
     fun insertUser(user: User): Observable<Boolean>

}
