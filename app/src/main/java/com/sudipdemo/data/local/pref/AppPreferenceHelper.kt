package com.sudipdemo.data.local.pref


import android.content.Context
import android.content.SharedPreferences
import com.sudipdemo.data.SudipDemoDataManager
import com.sudipdemo.di.PreferenceInfo
import javax.inject.Inject

class AppPreferenceHelper @Inject
constructor(context: Context, @PreferenceInfo prefFileName: String) : SudipDemoPrefHelper {


    override var phone: String
        get() = mPrefs.getString(PREF_KEY_PHONE_NUMBER, "")!!
        set(value) {
            mPrefs.edit().putString(PREF_KEY_PHONE_NUMBER, value).apply()
        }



    private val mPrefs: SharedPreferences

    override var passWord: String
        get() = mPrefs.getString(PREF_KEY_PASSWORD, "")!!
        set(value) {
            mPrefs.edit().putString(PREF_KEY_PASSWORD, value).apply()
        }

    override var timeZone: String
        get() = mPrefs.getString(PREF_KEY_TIMEZONE, "")!!
        set(value) {
            mPrefs.edit().putString(PREF_KEY_TIMEZONE, value).apply()
        }
    override var accessToken: String
        get() = mPrefs.getString(PREF_KEY_ACCESS_TOKEN, "")!!
        set(accessToken) = mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply()

    override var currentUserEmail: String
        get() = mPrefs.getString(PREF_KEY_CURRENT_USER_EMAIL, "")!!
        set(email) = mPrefs.edit().putString(PREF_KEY_CURRENT_USER_EMAIL, email).apply()

    override var currentUserId: String
        get() {
//            val userId = mPrefs.getLong(PREF_KEY_CURRENT_USER_ID, AppConstants.NULL_INDEX)
            return mPrefs.getString(PREF_KEY_CURRENT_USER_ID, "")!!
        }
        set(userId) {
//            val id = userId ?: AppConstants.NULL_INDEX
            mPrefs.edit().putString(PREF_KEY_CURRENT_USER_ID, userId).apply()
        }

    override val currentUserLoggedInMode: Int
        get() = mPrefs.getInt(PREF_KEY_USER_LOGGED_IN_MODE,
                SudipDemoDataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.type)

    override var currentUserName: String
        get() = mPrefs.getString(PREF_KEY_CURRENT_USER_NAME, "")!!
        set(userName) = mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NAME, userName).apply()

    override var currentUserProfilePicUrl: String
        get() = mPrefs.getString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, "")!!
        set(profilePicUrl) = mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, profilePicUrl).apply()

    init {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)
    }


    override fun setCurrentUserLoggedInMode(mode: SudipDemoDataManager.LoggedInMode) {
        mPrefs.edit().putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.type).apply()
    }

    companion object {

        private val PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"

        private val PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL"

        private val PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID"

        private val PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME"

        private val PREF_KEY_CURRENT_USER_PROFILE_PIC_URL = "PREF_KEY_CURRENT_USER_PROFILE_PIC_URL"

        private val PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE"

        private val PREF_KEY_TIMEZONE = "timezone"

        private val PREF_KEY_PASSWORD = "PREF_KEY_PASSWORD"

        private val PREF_KEY_PHONE_NUMBER = "PREF_KEY_PHONE_NUMBER"

    }
}
