package com.sudipdemo.data.local.pref


import com.sudipdemo.data.SudipDemoDataManager

interface SudipDemoPrefHelper {

    var accessToken: String

    var currentUserEmail: String

    var currentUserId: String

    val currentUserLoggedInMode: Int

    var currentUserName: String

    var currentUserProfilePicUrl: String

    var timeZone:String

    var passWord:String


    var phone:String





    fun setCurrentUserLoggedInMode(mode: SudipDemoDataManager.LoggedInMode)
}
