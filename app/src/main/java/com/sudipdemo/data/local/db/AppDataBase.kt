package com.sudipdemo.data.local.db


import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.sudipdemo.data.local.db.dao.UserDao
import com.sudipdemo.data.model.db.User
import com.sudipdemo.utils.DateConverter


@Database(entities = [User::class], version = 1, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class AppDataBase : RoomDatabase() {

    abstract fun userDao(): UserDao

}
