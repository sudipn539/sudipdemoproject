package com.sudipdemo.data

import android.content.Context
import com.google.gson.Gson
import com.sudipdemo.data.local.db.DbHelper
import com.sudipdemo.data.local.pref.AppPreferenceHelper
import com.sudipdemo.data.model.api.*
import com.sudipdemo.data.model.db.User
import com.sudipdemo.data.remote.ApiHeader
import com.sudipdemo.data.remote.ApiHelper
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton

class AppDataManager @Inject
constructor(private val mApiHelper: ApiHelper, private val mContext: Context, private val mDbHelper: DbHelper, private val mGson: Gson, private val mPreferencesHelper: AppPreferenceHelper) : SudipDemoDataManager {

    override var phone: String
        get() = mPreferencesHelper.phone
        set(value) {
            mPreferencesHelper.phone=value
        }






//    override fun updateLocation(id: String, lat: String, longi: String): Observable<Boolean> {
//        return mDbHelper.updateLocation(id, lat, longi)
//    }



    override var passWord: String
        get() = mPreferencesHelper.passWord
        set(value) {
            mPreferencesHelper.passWord=value
        }



    override var timeZone: String
        get() = mPreferencesHelper.timeZone
        set(value) {
            mPreferencesHelper.timeZone=value
        }




    override val allUsers: Observable<List<User>>
        get() = mDbHelper.allUsers

    override var accessToken: String
        get() = mPreferencesHelper.accessToken
        set(accessToken) {

            mPreferencesHelper.accessToken = accessToken
        }

    override var currentUserEmail: String
        get() = mPreferencesHelper.currentUserEmail
        set(email) {
            mPreferencesHelper.currentUserEmail = email
        }

    override var currentUserId: String
        get() = mPreferencesHelper.currentUserId
        set(userId) {
            mPreferencesHelper.currentUserId = userId
        }

    override val currentUserLoggedInMode: Int
        get() = mPreferencesHelper.currentUserLoggedInMode

    override var currentUserName: String
        get() = mPreferencesHelper.currentUserName
        set(userName) {
            mPreferencesHelper.currentUserName = userName
        }

    override var currentUserProfilePicUrl: String
        get() = mPreferencesHelper.currentUserProfilePicUrl
        set(profilePicUrl) {
            mPreferencesHelper.currentUserProfilePicUrl = profilePicUrl
        }

    override val apiHeader: ApiHeader
        get() = mApiHelper.apiHeader

    override fun setUserAsLoggedOut() {
        updateUserInfo("", "", SudipDemoDataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT, "", "", "","","","","")
    }

    override fun updateApiHeader(userId: String, accessToken: String) {
        mApiHelper.apiHeader.protectedApiHeader.userId = userId
        mApiHelper.apiHeader.protectedApiHeader.accessToken = accessToken
    }

    override fun updateUserInfo(accessToken: String, userId: String, loggedInMode: SudipDemoDataManager.LoggedInMode, userName: String, email: String, profilePicPath: String, passWord: String, fiscalcode: String, idcardNo: String, phone: String) {
        this.accessToken = accessToken
        currentUserId = userId
        setCurrentUserLoggedInMode(loggedInMode)
        currentUserName = userName
        currentUserEmail = email
        currentUserProfilePicUrl = profilePicPath
        this.passWord=passWord
        this.phone=phone

        updateApiHeader(userId, accessToken)
    }

    override fun insertUser(user: User): Observable<Boolean> {
        return mDbHelper.insertUser(user)
    }


    override fun setCurrentUserLoggedInMode(mode: SudipDemoDataManager.LoggedInMode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode)
    }







}
