package com.sudipdemo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ViewModelProviderFactory<V:Any>() : ViewModelProvider.Factory {

    private lateinit var viewModel: V

    constructor(viewModel: V) : this() {
        this.viewModel = viewModel

    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(viewModel.javaClass)) {
            return  viewModel as T;
        }
        throw IllegalArgumentException("Unknown class name")
    }
}

