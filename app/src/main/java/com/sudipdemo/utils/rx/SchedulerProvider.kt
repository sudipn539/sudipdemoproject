package com.sudipdemo.utils.rx

import io.reactivex.Scheduler


interface SchedulerProvider {

    fun from(n:Int): Scheduler
    fun trampoline(): Scheduler
    fun single(): Scheduler
    fun computation(): Scheduler

    fun io(): Scheduler

    fun ui(): Scheduler
    fun error()
}
