package com.sudipdemo.utils

import androidx.databinding.ObservableField
import io.reactivex.Observable

object FieldUtils {

    fun <T> toObservable(field: ObservableField<T>): Observable<T> {

        return Observable.create { e ->
            val initialValue = field.get()
            if (initialValue != null) {
                e.onNext(initialValue)
            }
            val callback = object : androidx.databinding.Observable.OnPropertyChangedCallback() {
                override fun onPropertyChanged(observable: androidx.databinding.Observable, i: Int) {
                    e.onNext(field.get()!!)
                }
            }
            field.addOnPropertyChangedCallback(callback)
            e.setCancellable { field.removeOnPropertyChangedCallback(callback) }
        }
    }

    /**
     * A convenient wrapper for `ReadOnlyField#create(Observable)`
     * @return DataBinding field created from the specified Observable
     */
    fun <T> toField(observable: Observable<T>): ReadOnlyField<T> {
        return ReadOnlyField.create(observable)
    }

}
