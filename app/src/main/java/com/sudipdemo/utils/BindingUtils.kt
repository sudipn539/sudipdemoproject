package com.sudipdemo.utils

import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingListener
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions


object BindingUtils {

    val options = RequestOptions.bitmapTransform(GlideCircleBorderTransform(2f , Color.LTGRAY))
            .diskCacheStrategy(DiskCacheStrategy.DATA)



    @JvmStatic
    @BindingAdapter("customText")
    fun setCustomText(textView: TextView, url: String?) {
     textView.setText(url)
    }


    @JvmStatic
    @BindingAdapter("drawableImage")
    fun setImageDrawable(imageView: ImageView, url: Int?) {
        //        Log.i("ImageViewurl",url);
        val context = imageView.context
        if (url != null)
        //            Glide.with(context).load(url).apply(RequestOptions.fitCenterTransform()).into(imageView);
            imageView.setImageResource(url)
        else {
            //            Glide.with(context).load(R.drawable.greenedge).apply(RequestOptions.fitCenterTransform()).into(imageView);
            imageView.setImageResource(url!!)
        }
    }



    @BindingAdapter(value = ["realValueAttrChanged"])
    fun setListener(editText: EditText, listener: InverseBindingListener?) {
        if (listener != null) {
            editText.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(editable: Editable) {
                    listener.onChange()
                }
            })
        }
    }
    @JvmStatic
    @BindingAdapter("setWebViewClient")
    fun setWebViewClient(view: WebView, client: WebViewClient) {
        view.webViewClient = client
    }
    @JvmStatic
    @BindingAdapter("loadUrl")
    fun setloadUrl(webView: WebView, url: String?) {
        //        Log.i("ImageViewurl",url);
        val context = webView.context
        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        if(url!=null)
        webView.loadData(url, "text/html", "UTF-8")

    }



}// This class is not publicly instantiable
