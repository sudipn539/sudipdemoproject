package com.sudipdemo.utils;

enum class Status {
    SUCCESS,
    FAIL,
    ERROR,
    LOADING
}