package com.sudipdemo.utils

object AppConstants {
    const val DB_NAME = "sudipDemo.db"
    const val DEVICE_TYPE = "1"


    val NULL_INDEX = -1L
    val TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss"

    val PREF_NAME = "sudipDemo_pref"






}// This utility class is not publicly instantiable
