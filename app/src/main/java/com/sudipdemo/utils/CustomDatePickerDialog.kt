package com.sudipdemo.utils

import android.app.DatePickerDialog
import android.content.Context
import android.os.Build
import android.widget.DatePicker
import io.reactivex.annotations.NonNull
import java.util.*

class CustomDatePickerDialog(context: Context, callBack: DatePickerDialog.OnDateSetListener, year: Int, monthOfYear: Int, dayOfMonth: Int) : DatePickerDialog(context, callBack, year, monthOfYear, dayOfMonth) {

    private var maxYear: Int = 0
    private var maxMonth: Int = 0
    private var maxDay: Int = 0

    fun setMaxDate(maxDate: Long) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //            getDatePicker().setMaxDate(System.currentTimeMillis());
            datePicker.maxDate = maxDate
            //            getDatePicker().setMinDate(13);
        } else {
            val c = Calendar.getInstance()
            c.timeInMillis = maxDate
            maxYear = c.get(Calendar.YEAR)
            maxMonth = c.get(Calendar.MONTH)
            maxDay = c.get(Calendar.DAY_OF_MONTH)
        }
    }

    fun setMinDate(maxDate: Long) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            datePicker.minDate = maxDate
        } else {
            /*final Calendar c = Calendar.getInstance();
            c.setTimeInMillis(maxDate);
            maxYear = c.get(Calendar.YEAR);
            maxMonth = c.get(Calendar.MONTH);
            maxDay = c.get(Calendar.DAY_OF_MONTH);*/
        }
    }

    override fun onDateChanged(@NonNull view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            super.onDateChanged(view, year, monthOfYear, dayOfMonth)
        } else {
            if (year < maxYear)
                view.updateDate(maxYear, maxMonth, maxDay)

            if (monthOfYear < maxMonth && year == maxYear)
                view.updateDate(maxYear, maxMonth, maxDay)

            if (dayOfMonth < maxDay && year == maxYear && monthOfYear == maxMonth)
                view.updateDate(maxYear, maxMonth, maxDay)
        }
    }

}

