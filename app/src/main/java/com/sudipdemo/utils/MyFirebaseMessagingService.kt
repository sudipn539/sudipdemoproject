package com.sudipdemo.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_HIGH
import androidx.core.content.ContextCompat
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.sudipdemo.SudipDemo
import com.sudipdemo.R
import com.sudipdemo.data.AppDataManager
import com.sudipdemo.di.component.AppComponent
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import javax.inject.Inject


//
class MyFirebaseMessagingService: FirebaseMessagingService()  {
var notificationCount=0
@Inject
internal lateinit var appDatamanager: AppDataManager
    var mAppComponent: AppComponent?=null



    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        mAppComponent= SudipDemo.mInstance.mAppComponent
        mAppComponent!!.inject(this)
   
        Log.d(TAG, "From: ${remoteMessage.from}")

        // Check if message contains a data payload.
        remoteMessage.data.isNotEmpty().let {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)



            when(remoteMessage.data.get("type")){
                "1"->{
                    val sendPush = Intent()
                    sendPush.action = "NotificationActivity"
                    sendBroadcast(sendPush)
                    sendNotification(remoteMessage)
                }
                "2"->{

                        val broadcast = Intent()
                        broadcast.action = "msg"
                        broadcast.putExtra("message" , remoteMessage.data.get("message"))
//                    broadcast.putExtra("message" , remoteMessage.data.get("message"))
                        sendBroadcast(broadcast)

                }
            }

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob()
            } else {
                // Handle message within 10 seconds
                handleNow()
            }
        }

        // Check if message contains a notification payload.
//        remoteMessage?.notification?.let {
//            Log.d(TAG, "Message Notification Body: ${it.body}")
//
//            sendNotification(remoteMessage)
//        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
// [END receive_message]

// [START on_new_token]
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token)
    }
// [END on_new_token]

    /**
     * Schedule async work using WorkManager.
     */
    private fun scheduleJob() {
        // [START dispatch_job]

        val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
        WorkManager.getInstance(this).beginWith(work).enqueue()
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */

    private fun sendNotification(remoteMessage: RemoteMessage) {

//        or Intent.FLAG_ACTIVITY_SINGLE_TOP

//        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                0)


        val channelId = getString(R.string.app_name)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher))
                .setColor(ContextCompat.getColor(this,R.color.notification_background))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(remoteMessage.data.get("message").toString())
                .setAutoCancel(true)
                .setDefaults(remoteMessage.data.get("vibrate")!!.toInt())
                .setSound(defaultSoundUri)
              //  .setContentIntent(pendingIntent)
                .setNumber(remoteMessage.data.get("badgecount")!!.toInt())
                .setPriority(PRIORITY_HIGH)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }
        Log.d("notificationCount check","--"+(++notificationCount))
            notificationManager.notify(++notificationCount /* ID of notification */, notificationBuilder.build())

    }

    companion object {

        private const val TAG = "MyFirebaseMsgService"
    }
}