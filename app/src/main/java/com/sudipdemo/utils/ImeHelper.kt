package com.sudipdemo.utils

import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.RestrictTo

@RestrictTo(RestrictTo.Scope.LIBRARY_GROUP)
object ImeHelper {
    interface DonePressedListener {
        fun onDonePressed()
    }

    fun setImeOnDoneListener(doneEditText: EditText,
                             listener: DonePressedListener) {
        doneEditText.setOnEditorActionListener(TextView.OnEditorActionListener { view, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                listener.onDonePressed()
                return@OnEditorActionListener true
            }
            false
        })
    }
}
