package com.sudipdemo.utils


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.provider.Settings
import android.view.Gravity
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.sudipdemo.R
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


object CommonUtils {

    val timestamp: String
        get() = SimpleDateFormat(AppConstants.TIMESTAMP_FORMAT, Locale.US).format(Date())

    @SuppressLint("all")
    fun getDeviceId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun isEmailValid(email: String): Boolean {
        val EMAIL_PATTERN = "^[A-Za-z0-9\\+]+([\\.|\\_][A-Za-z0-9]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(EMAIL_PATTERN)
        val matcher = pattern.matcher(email)
        return matcher.matches()
//        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    @Throws(IOException::class)
    fun loadJSONFromAsset(context: Context, jsonFileName: String): String {
        val manager = context.assets
        val `is` = manager.open(jsonFileName)

        val size = `is`.available()
        val buffer = ByteArray(size)
        `is`.read(buffer)
        `is`.close()

        return String(buffer)
//        UTF-8
    }

    fun showLoadingDialog(context: Context): ProgressDialog {
        val progressDialog = ProgressDialog(context)
        progressDialog.show()
        if (progressDialog.window != null) {
            progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        progressDialog.setContentView(R.layout.progress_dialog)
        progressDialog.isIndeterminate = true
        progressDialog.setCancelable(false)
        progressDialog.setCanceledOnTouchOutside(false)
        return progressDialog
    }

    @SuppressLint("NewApi")
    fun showLoadingDialog1(context: Context): Dialog {
        val progressDialog = Dialog(context)
        progressDialog.show()
        if (progressDialog.window != null) {
            progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        progressDialog.setContentView(R.layout.progress_dialog)
//        progressDialog.isIndeterminate = true
        progressDialog.setCancelable(false)
        progressDialog.setCanceledOnTouchOutside(false)

        return progressDialog
    }


    fun setProgressDialog(context: Context):AlertDialog {
        val llPadding = 30
        val ll = LinearLayout(context)
        ll.setOrientation(LinearLayout.HORIZONTAL)
        ll.setPadding(llPadding , llPadding , llPadding , llPadding)
        ll.setGravity(Gravity.CENTER)
        var llParam: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT ,
                LinearLayout.LayoutParams.WRAP_CONTENT)
        llParam.gravity = Gravity.CENTER
        ll.setLayoutParams(llParam)
        val progressBar = ProgressBar(context)
        progressBar.setIndeterminate(true)
        progressBar.setPadding(0 , 0 , llPadding , 0)
        progressBar.setLayoutParams(llParam)
        llParam = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT ,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        llParam.gravity = Gravity.CENTER

        ll.addView(progressBar)

        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setCancelable(true)
        builder.setView(ll)
        val dialog: AlertDialog = builder.create()

        dialog.show()
        val window: Window = dialog.getWindow()!!
        if (window != null) {
            val layoutParams: WindowManager.LayoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog.getWindow()!!.getAttributes())
            layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
            layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            dialog.getWindow()!!.setAttributes(layoutParams)
        }
        return dialog
    }


    @Throws(ParseException::class)
    fun TimeStampConverter(inputFormat: String,
                           inputTimeStamp: String, outputFormat: String): String {
        return SimpleDateFormat(outputFormat).format(SimpleDateFormat(
                inputFormat).parse(inputTimeStamp))
    }
     fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }
    fun dp2px(context: Context , dp: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }
    @JvmStatic
    fun getWidth(context: Context): Int {
        val size = Point()
        val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowmanager.getDefaultDisplay().getSize(size)
//        val displayMetrics = DisplayMetrics()
//        val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
//        windowmanager.defaultDisplay.getMetrics(displayMetrics)
        return size.x
    }
    @JvmStatic
    fun getHight(context: Context): Int {
        val size = Point()
        val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowmanager.getDefaultDisplay().getSize(size)
//        val displayMetrics = DisplayMetrics()
//        val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
//        windowmanager.defaultDisplay.getMetrics(displayMetrics)
        return size.y
    }



}// This utility class is not publicly instantiable
