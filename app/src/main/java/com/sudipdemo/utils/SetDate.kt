package com.sudipdemo.utils

import android.app.DatePickerDialog
import android.content.Context
import android.view.View
import android.widget.DatePicker
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*

class SetDate(private val editText: TextView, private val ctx: Context) : View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener {
    private val myCalendar: Calendar

    init {
        this.editText.onFocusChangeListener = this
        myCalendar = Calendar.getInstance()
    }

    override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        // this.editText.setText();

        val myFormat = "MMM dd, yyyy" //In which you need put here
        val sdformat = SimpleDateFormat(myFormat, Locale.US)
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        editText.text = sdformat.format(myCalendar.time)

    }

    override fun onFocusChange(v: View, hasFocus: Boolean) {
        // TODO Auto-generated method stub
        if (hasFocus) {
            DatePickerDialog(ctx, this, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }
    }

}