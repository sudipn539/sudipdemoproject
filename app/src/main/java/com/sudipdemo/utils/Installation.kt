//package com.boomn.utils
//
//import android.content.Context
//
//import java.io.File
//import java.io.FileOutputStream
//import java.io.IOException
//import java.io.RandomAccessFile
//import java.math.BigInteger
//import java.security.MessageDigest
//import java.security.NoSuchAlgorithmException
//import java.util.UUID
//
//object Installation {
//    private var sID: String? = null
//    private val INSTALLATION = "INSTALLATION"
//
//    @Synchronized
//    fun id(context: Context): String? {
//        if (sID == null) {
//            val installation = File(context.filesDir.absolutePath, INSTALLATION)
////            context.getFilesDir().getAbsolutePath();
////            val v=File(context.filesDir,INSTALLATION)
//            try {
//                if (!installation.exists())
//                    writeInstallationFile(installation)
//                sID = readInstallationFile(installation)
//            } catch (e: Exception) {
//                throw RuntimeException(e)
//            }
//
//        }
//        return encodeToMD5(sID!!)
//    }
//
//    @Throws(IOException::class)
//    private fun readInstallationFile(installation: File): String {
//        val f = RandomAccessFile(installation, "r")
//        val bytes = ByteArray(f.length().toInt())
//        f.readFully(bytes)
//        f.close()
//        return String(bytes)
//    }
//
//    @Throws(IOException::class)
//    private fun writeInstallationFile(installation: File) {
//        val out = FileOutputStream(installation)
//        val id = UUID.randomUUID().toString()
//        out.write(id.toByteArray())
//        out.close()
//    }
//
//
//    private fun encodeToMD5(source: String): String? {
//        var result: String? = null
//        val m: MessageDigest
//        try {
//            m = MessageDigest.getInstance("MD5")
//            m.update(source.toByteArray(), 0, source.length)
//            result = BigInteger(1, m.digest()).toString(16)
//        } catch (e1: NoSuchAlgorithmException) {
//            e1.printStackTrace()
//        }
//
//        return result
//    }
//}


package com.sudipdemo.utils

import android.content.Context
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.RandomAccessFile
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

object Installation {
    private var sID: String? = null
    private val INSTALLATION = "INSTALLATION"

    @Synchronized
    fun id(context: Context): String? {
        if (sID == null) {
            Log.d("context.filesDir",""+context.applicationContext)
            val installation = File(context.filesDir, INSTALLATION)
            try {
                if (!installation.exists())
                    writeInstallationFile(installation)
                sID = readInstallationFile(installation)
            } catch (e: Exception) {
                throw RuntimeException(e)
            }

        }
        return encodeToMD5(sID!!)
    }

    @Throws(IOException::class)
    private fun readInstallationFile(installation: File): String {
        val f = RandomAccessFile(installation, "r")
        val bytes = ByteArray(f.length().toInt())
        f.readFully(bytes)
        f.close()
        return String(bytes)
    }

    @Throws(IOException::class)
    private fun writeInstallationFile(installation: File) {
        val out = FileOutputStream(installation)
        val id = UUID.randomUUID().toString()
        out.write(id.toByteArray())
        out.close()
    }


    private fun encodeToMD5(source: String): String? {
        var result: String? = null
        val m: MessageDigest
        try {
            m = MessageDigest.getInstance("MD5")
            m.update(source.toByteArray(), 0, source.length)
            result = BigInteger(1, m.digest()).toString(16)
        } catch (e1: NoSuchAlgorithmException) {
            e1.printStackTrace()
        }

        return result
    }
}

