
package com.sudipdemo.utils;

public interface OnOtpCompletionListener {
  void onOtpCompleted(String otp);

}
