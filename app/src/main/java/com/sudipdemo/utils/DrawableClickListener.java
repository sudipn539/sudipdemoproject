package com.sudipdemo.utils;

public interface DrawableClickListener {
     enum DrawablePosition { TOP, BOTTOM, LEFT, RIGHT };
     void onClick(DrawablePosition target);
}
