package com.sudipdemo.utils

import android.util.Log
import androidx.databinding.ObservableField
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import java.util.*

class ReadOnlyField<T> private constructor(source: Observable<T>) : ObservableField<T>() {
    private val source: Observable<T>
    private val subscriptions = HashMap<androidx.databinding.Observable.OnPropertyChangedCallback, Disposable>()

    init {
        this.source = source.doOnNext { t -> super@ReadOnlyField.set(t) }.doOnError { throwable -> Log.e("ReadOnlyField", "onError in source observable", throwable) }.onErrorResumeNext(Observable.empty()).share()
    }


    @Deprecated("Setter of ReadOnlyField does nothing. Merge with the source Observable instead.")
    override fun set(value: T) {
    }

    @Synchronized
    override fun addOnPropertyChangedCallback(callback: androidx.databinding.Observable.OnPropertyChangedCallback) {
        super.addOnPropertyChangedCallback(callback)
        subscriptions.put(callback, source.subscribe())
    }

    @Synchronized
    override fun removeOnPropertyChangedCallback(callback: androidx.databinding.Observable.OnPropertyChangedCallback) {
        super.removeOnPropertyChangedCallback(callback)
        val subscription = subscriptions.remove(callback)
        if (subscription != null && !subscription!!.isDisposed()) {
            subscription!!.dispose()
        }
    }

    companion object {

        fun <U> create(source: Observable<U>): ReadOnlyField<U> {
            return ReadOnlyField(source)
        }
    }
}