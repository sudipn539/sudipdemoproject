package com.sudipdemo.utils

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.annotation.*
import com.sudipdemo.SudipDemo


class ResourceUtils {

       var mContext: Context? = null

     constructor(mContext: Context) {
          this.mContext = mContext
      }
     fun getString(@StringRes stringId: Int): String {
        if (Build.VERSION.SDK_INT > 22)
            return mContext!!.getString(stringId)
        else {
            return mContext!!.resources.getString(stringId)
        }
    }

    fun getDrawable(@DrawableRes drawableId: Int): Drawable? {
            return mContext!!.getDrawable(drawableId)

//        return Gowiith().getDrawable(drawableId)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun getColor(@ColorRes colorId: Int): Int {
            return mContext!!.getColor(colorId)

//        return Gowiith().getColor(colorId)
    }

    fun getDimen(@DimenRes dimenId: Int): Int {
        return SudipDemo().getResources().getDimension(dimenId).toInt()
    }

    fun dpToPx(dp: Int): Int {
        return (dp * Resources.getSystem().displayMetrics.density).toInt()
    }

    fun pxToDp(px: Int): Int {
        return (px / Resources.getSystem().displayMetrics.density).toInt()
    }

}
