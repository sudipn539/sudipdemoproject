package com.sudipdemo.utils

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters

class MyWorker(appContext: Context , param:WorkerParameters):Worker(appContext,param) {
    override fun doWork(): Result {
        return ListenableWorker.Result.success()
    }
    companion object{
        private val TAG="MyWorker"
    }
}