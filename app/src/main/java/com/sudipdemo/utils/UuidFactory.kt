package com.sudipdemo.utils

import android.content.Context
import android.provider.Settings.Secure
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


object UuidFactory {
    fun getUuId(cnt: Context): String? {
        val result: String?

        val androidId = Secure.ANDROID_ID

//        val androidId = Secure.getString(, Secure.ANDROID_ID)
        println("ANDROID_ID=$androidId")
        // get device id
        val deviceId = Installation.id(cnt)

        println("deviceId=$deviceId")
        // split android ID in to two parts
        val androidIdPart1 = androidId.substring(0, androidId.length / 2)
        val androidIdPart2 = androidId.substring(androidId.length / 2)

        // split device ID in to two parts
        val deviceIdPart1 = deviceId!!.substring(0, deviceId.length / 2)
        val deviceIdPart2 = deviceId.substring(deviceId.length / 2)

        // mix parts
        val finalIdString = androidIdPart1 + deviceIdPart2 + deviceIdPart1 + androidIdPart2

        // encode string to md5
        //result = "d896d51e453358743550670e1835aea";
        result = encodeToMD5(finalIdString)


        return result
    }

    /**
     *
     * @param source
     * @return
     */
    private fun encodeToMD5(source: String): String? {
        var result: String? = null
        val m: MessageDigest
        try {
            m = MessageDigest.getInstance("MD5")
            m.update(source.toByteArray(), 0, source.length)
            result = BigInteger(1, m.digest()).toString(16)
        } catch (e1: NoSuchAlgorithmException) {
            e1.printStackTrace()
        }

        return result
    }
}
