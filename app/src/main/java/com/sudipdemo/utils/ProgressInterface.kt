package com.sudipdemo.utils

interface ProgressInterface {
     fun onProgress(bytesUploaded: Long , totalBytes: Long)
}