package com.sudipdemo.utils

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class EndlessRecyclerOnScrollListener : RecyclerView.OnScrollListener {

    private var previousTotal = 0 // The total number of items in the dataset after the last load
    private var loading = true // True if we are still waiting for the last set of data to load.
    private var visibleThreshold = 5 // The minimum amount of items to have below your current scroll position before loading more.
    internal var firstVisibleItem: Int = 0
    internal var visibleItemCount: Int = 0
    internal var totalItemCount: Int = 0

    private var currentPage = 1

    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var isUseLinearLayoutManager: Boolean=false
    private var isUseGridLayoutManager: Boolean=false

    constructor(linearLayoutManager: LinearLayoutManager) {
        this.mLayoutManager = linearLayoutManager
        isUseLinearLayoutManager = true

    }

    constructor(gridLayoutManager: GridLayoutManager) {
        this.mLayoutManager = gridLayoutManager
        isUseGridLayoutManager = true
        visibleThreshold = 6

    }

   override fun onScrolled(recyclerView: RecyclerView , dx: Int , dy: Int) {
        super.onScrolled(recyclerView , dx , dy)

        visibleItemCount = recyclerView.getChildCount()
        totalItemCount = mLayoutManager!!.getItemCount()


        if (isUseLinearLayoutManager && mLayoutManager is LinearLayoutManager) {
            firstVisibleItem = (mLayoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        }

        if (isUseGridLayoutManager && mLayoutManager is GridLayoutManager) {
            firstVisibleItem = (mLayoutManager as GridLayoutManager).findFirstVisibleItemPosition()
        }

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }
        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) {
            // End has been reached

            // Do something
            currentPage++

            onLoadMore(currentPage)

            loading = true
        }

    }

    abstract fun onLoadMore(currentPage: Int)

    companion object {
        var TAG = EndlessRecyclerOnScrollListener::class.java.simpleName
    }
}