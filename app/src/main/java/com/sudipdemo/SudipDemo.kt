package com.sudipdemo
//import dagger.android.HasActivityInjector

import android.app.Application
import android.content.Context
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.gsonparserfactory.GsonParserFactory
import com.facebook.stetho.Stetho
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import com.sudipdemo.di.component.AppComponent
import com.sudipdemo.di.component.DaggerAppComponent
import com.sudipdemo.utils.ResourceUtils
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import okhttp3.OkHttpClient
import java.util.*
import javax.inject.Inject


class SudipDemo : Application() , HasAndroidInjector {
    var mAppComponent:AppComponent?=null
    var mCartCount = 0
    internal  var token: String=""
//    var mInstance: Gowiith? = null
    lateinit var context: Context
    @Inject
    internal lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Any>


    override fun androidInjector(): AndroidInjector<Any>? = activityDispatchingAndroidInjector

       var  mResourceProvider : ResourceUtils?=null
      fun  getResourceProvider():ResourceUtils {
        if (mResourceProvider == null)
            mResourceProvider =ResourceUtils(this)
        return mResourceProvider!!
    }

    companion object {
     lateinit  var mInstance: SudipDemo

    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this
        context=this
      mAppComponent=DaggerAppComponent.builder()
                .application(this)
                .build()
        mAppComponent!!.inject(this)

        val timezoneID = TimeZone.getDefault().id
        println("timezone $timezoneID")
        Stetho.initializeWithDefaults(this)
        val okHttpClient = OkHttpClient().newBuilder()
                .addNetworkInterceptor(StethoInterceptor())
                .build()

//        val languageToLoad = "it" // your language
//
//        val locale = Locale(languageToLoad)
//        Locale.setDefault(locale)
//        val config = Configuration()
//        config.locale = locale
//        baseContext.resources.updateConfiguration(config,
//                baseContext.resources.displayMetrics)


//        val client = OkHttpClient.Builder()

//        val interceptor = HttpLoggingInterceptor()
//        interceptor.level = HttpLoggingInterceptor.Level.BODY
//        client.addInterceptor(interceptor)
        AndroidNetworking.initialize(applicationContext)
        AndroidNetworking.initialize(this,okHttpClient)
        val gson = GsonBuilder().setLenient().create()
        AndroidNetworking.setParserFactory(GsonParserFactory(gson))
//        AndroidNetworking.enableLogging()
        if (BuildConfig.DEBUG) {
//            AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY);
//            AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BASIC);
            getTimezone()
        }
//        try {
//            val info = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
//            for (signature in info.signatures) {
//                val md = MessageDigest.getInstance("SHA")
//                md.update(signature.toByteArray())
//                val hashKey = String(Base64.encode(md.digest(), 0))
//                Log.i("TAG", "printHashKey() Hash Key: $hashKey")
//            }
//        } catch (e: NoSuchAlgorithmException) {
//            Log.e("TAG", "printHashKey()", e)
//        } catch (e: Exception) {
//            Log.e("TAG", "printHashKey()", e)
//        }
//        FirebaseApp.initializeApp(this)
//        FirebaseInstanceId.getInstance().instanceId
//                .addOnCompleteListener { task ->
//                    if (!task.isSuccessful) {
//
//                        Log.d("isSuccessful","=="+token)
//                        token=""
//                        return@addOnCompleteListener
//                    }
//                    // Get new Instance ID token
//                     token = task.result!!.token
//                    Log.d("token","=="+token)
////                    setToken(token)
//                }

    }


    fun getTimezone(): String {
        val timezoneID = TimeZone.getDefault().id
        println("timezone $timezoneID")
        return timezoneID
    }

//    override fun attachBaseContext(base: Context?) {
//        val languageHelper:LanguageHelper= LanguageHelper()
//        super.attachBaseContext(base?.let { languageHelper.onAttach(it, languageHelper.getLanguage(base)) })
//    }

}


