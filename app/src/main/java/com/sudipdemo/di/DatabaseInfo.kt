package com.sudipdemo.di

import javax.inject.Qualifier


@Qualifier
//@Retention(RetentionPolicy.RUNTIME)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class DatabaseInfo
