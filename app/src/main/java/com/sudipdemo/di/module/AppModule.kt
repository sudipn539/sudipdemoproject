package com.sudipdemo.di.module


import android.app.Application
import android.content.Context
import androidx.room.Room
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.sudipdemo.BuildConfig
import com.sudipdemo.data.AppDataManager
import com.sudipdemo.data.SudipDemoDataManager
import com.sudipdemo.data.local.db.AppDataBase
import com.sudipdemo.data.local.db.AppDbHelper
import com.sudipdemo.data.local.db.DbHelper
import com.sudipdemo.data.local.pref.AppPreferenceHelper
import com.sudipdemo.data.local.pref.SudipDemoPrefHelper
import com.sudipdemo.data.remote.ApiHeader
import com.sudipdemo.data.remote.ApiHelper
import com.sudipdemo.data.remote.AppApiHelper
import com.sudipdemo.di.ApiInfo
import com.sudipdemo.di.DatabaseInfo
import com.sudipdemo.di.PreferenceInfo
import com.sudipdemo.utils.AppConstants
import com.sudipdemo.utils.rx.SchedulerProvider
import com.sudipdemo.utils.rx.SeedErpSchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider {
        return SeedErpSchedulerProvider()
    }


    @Provides
    @Singleton
    internal fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper {
        return appApiHelper
    }

    @Provides
    @ApiInfo
    internal fun provideApiKey(): String {
        return BuildConfig.API_KEY
    }

    @Provides
    @Singleton
    internal fun provideAppDatabase(@DatabaseInfo dbName: String, context: Context): AppDataBase {
        return Room.databaseBuilder(context, AppDataBase::class.java, dbName).fallbackToDestructiveMigration()
                .build()
    }


    @Provides
    @Singleton
    internal fun provideDataManager(appDataManager: AppDataManager): SudipDemoDataManager {
        return appDataManager
    }

    @Provides
    @DatabaseInfo
    internal fun provideDatabaseName(): String {
        return AppConstants.DB_NAME
    }

    @Provides
    @Singleton
    internal fun provideDbHelper(appDbHelper: AppDbHelper): DbHelper {
        return appDbHelper
    }

    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        return GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
    }

    @Provides
    @PreferenceInfo
    internal fun providePreferenceName(): String {
        return AppConstants.PREF_NAME
    }

    @Provides
    @Singleton
    internal fun providePreferencesHelper(appPreferencesHelper: AppPreferenceHelper): SudipDemoPrefHelper {
        return appPreferencesHelper
    }

    @Provides
    @Singleton
    internal fun provideProtectedApiHeader(@ApiInfo apiKey: String,
                                           preferencesHelper: SudipDemoPrefHelper): ApiHeader.ProtectedApiHeader {
        return ApiHeader.ProtectedApiHeader(
                apiKey,
                preferencesHelper.currentUserId,
                preferencesHelper.accessToken,
                preferencesHelper.timeZone)
    }


}
