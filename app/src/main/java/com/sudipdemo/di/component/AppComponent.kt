package com.sudipdemo.di.component


import android.app.Application
import com.sudipdemo.SudipDemo
import com.sudipdemo.di.builder.ActivityBuilder
import com.sudipdemo.di.module.AppModule
import com.sudipdemo.utils.MyFirebaseMessagingService
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [AndroidInjectionModule::class, ActivityBuilder::class, AppModule::class])
interface AppComponent {

    fun inject(app: SudipDemo)
    fun inject(service: MyFirebaseMessagingService)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}

