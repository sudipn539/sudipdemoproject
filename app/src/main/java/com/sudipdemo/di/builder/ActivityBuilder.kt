package com.sudipdemo.di.builder


import com.sudipdemo.ui.adduser.AddUserActivity
import com.sudipdemo.ui.adduser.AddUserModule
import com.sudipdemo.ui.firstscreen.FirstScreenActivity
import com.sudipdemo.ui.firstscreen.FirstScreenModule

import com.sudipdemo.ui.splash.SplashActivity
import com.sudipdemo.ui.splash.SplashModule
import com.sudipdemo.ui.userlist.UserListActivity
import com.sudipdemo.ui.userlist.UserListModule


import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [SplashModule::class])
    internal abstract fun splash(): SplashActivity




    @ContributesAndroidInjector(modules = [FirstScreenModule::class])
    internal abstract fun firstscreen(): FirstScreenActivity

    @ContributesAndroidInjector(modules = [AddUserModule::class])
    internal abstract fun addUser(): AddUserActivity
    @ContributesAndroidInjector(modules = [UserListModule::class])
    internal abstract fun userList(): UserListActivity


}
